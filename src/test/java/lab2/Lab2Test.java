package lab2;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;

import java.io.*;

import org.junit.jupiter.api.Test;

class Lab2Test {
	@Test
	void test1() {
		String[] argc = {"test.txt"};
		PrintStream consoleStream = System.out;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		PrintStream stream = new PrintStream(outputStream);
		System.setOut(stream);
		Lab2.main(argc);
		String result = outputStream.toString();
		System.setOut(consoleStream);
		assertEquals("1.0\n3.0\n-2.0\n2.0\n0.0\n",result);
	} 
}


		
