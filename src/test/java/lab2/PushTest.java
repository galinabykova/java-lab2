package lab2;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;

import org.junit.jupiter.api.Test;

class PushTest {
	@Test
	void test1() {
		Context context = new Context();
		Command a=new Push();
		String[] argc= {"PUSH","1.0"};
		try {
			a.make(argc, context);
		} catch (MyException e) {
		}
		assertEquals(1.0,context.getStack().getFirst());
	}
	@Test
	void test2() {
		Context context = new Context();
		Command a=new Push();
		String[] argc= {"PUSH","-1.0"};
		try {
			a.make(argc, context);
		} catch (MyException e) {
		}
		assertEquals(-1.0,context.getStack().getFirst());
	}
	@Test
	void test3() {
		Context context = new Context();
		String[] argc= {"DEFINE","a","5"};
		Command a=new Define();
		try {
			a.make(argc, context);
		} catch (MyException e) {
		}
		assertEquals(5.0,context.getPerems().get("a"));
		Command a1=new Push();
		String[] argc1= {"PUSH","a"};
		try {
			a1.make(argc, context);
		} catch (MyException e) {
		}
		assertEquals(5.0,context.getStack().getFirst());
	}
}
