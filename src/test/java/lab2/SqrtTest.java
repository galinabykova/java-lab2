package lab2;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;

import org.junit.jupiter.api.Test;

class SqrtTest {
	@Test
	void test1() {
		Context context = new Context();
		LinkedList <Double> stack=context.getStack();
		stack.addFirst(4.0);
		Command a=new Sqrt();
		String[] argc= {"SQRT"};
		try {
			a.make(argc, context);
		} catch (MyException e) {
		}
		assertEquals(2.0,context.getStack().getFirst());
	} 
	@Test
	void test2() {
		Context context = new Context();
		LinkedList <Double> stack=context.getStack();
		stack.addFirst(-4.0);
		Command a=new Sqrt();
		String[] argc= {"SQRT"+ ""};
		try {
			a.make(argc, context);
		} catch (MyException e) {
		}
		assertEquals(-4.0,context.getStack().getFirst());
	} 
}
