package lab2;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;

import org.junit.jupiter.api.Test;

class PopTest {

	@Test
	void test1() {
		Context context = new Context();
		LinkedList <Double> stack=context.getStack();
		stack.addFirst(1.0);
		stack.addFirst(2.0);
		Command a=new Pop();
		String[] argc= {"+"};
		try {
			a.make(argc, context);
		} catch (MyException e) {
		}
		assertEquals(1.0,context.getStack().getFirst());
	}
	@Test
	void test2() {
		Context context = new Context();
		LinkedList <Double> stack=context.getStack();
		stack.addFirst(1.0);
		Command a=new Pop();
		String[] argc= {"POP"};
		try {
			a.make(argc, context);
		} catch (MyException e) {
		}
		assertEquals(true,context.getStack().isEmpty());
	}
	@Test
	void test3() {
		Context context = new Context();
		Command a=new Pop();
		String[] argc= {"+"};
		try {
			a.make(argc, context);
		} catch (MyException e) {
		}
		assertEquals(true,context.getStack().isEmpty());
	}

}

