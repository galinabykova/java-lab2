package lab2;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class DefineTest {
	@Test
	void test1() {
		Context context = new Context();
		String[] argc= {"DEFINE","a","5"};
		Command a=new Define();
		try {
			a.make(argc, context);
		} catch (MyException e) {
		}
		assertEquals(5.0,context.getPerems().get("a"));
	}
	@Test
	void test2() {
		Context context = new Context();
		String[] argc= {"DEFINE","a_alfj","15"};
		Command a=new Define();
		try {
			a.make(argc, context);
		} catch (MyException e) {
		}
		assertEquals(15.0,context.getPerems().get("a_alfj"));
	}
}

