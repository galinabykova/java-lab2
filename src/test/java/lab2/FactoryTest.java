package lab2;

import static org.junit.jupiter.api.Assertions.*;

import java.util.LinkedList;

import org.junit.jupiter.api.Test;

class FactoryTest {

	@Test
	void test() {
		Factory factory = new Factory();
		factory.make("PUSH 1");
		factory.make("POP");
		factory.make("DEFINE a 3");
		factory.make("#a;gjk;ag");
		factory.make("PUSH 1");
		factory.make("PUSH 2");
		factory.make("+");
		factory.make("PUSH 1");
		factory.make("PUSH 2");
		factory.make("-");
		factory.make("PUSH 1");
		factory.make("PUSH 2");
		factory.make("*");
		factory.make("PUSH 1");
		factory.make("PUSH 2");
		factory.make("/");
		factory.make("PUSH 1");
		factory.make("PUSH 2");
		factory.make("SQRT");
		factory.make("PRINT");
	}

}
