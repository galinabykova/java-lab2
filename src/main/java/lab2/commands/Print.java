package lab2;

import java.util.logging.Logger;

class Print extends Command {
	private static Logger log=Logger.getLogger(Print.class.getName());
	void make (String[] argc,Context context) throws EmptyStack {
		log.info(argc.toString());
		Double p=context.getStack().peekFirst();
		if (p==null) throw new EmptyStack();
		else System.out.println(p);
		log.info("Print was completed");
	}
} 
