package lab2;

import java.util.logging.Logger;

class Plus extends Command {
	private static Logger log=Logger.getLogger(Plus.class.getName());
	void make (String [] argc,Context context) throws EmptyStack {
		log.info(argc.toString());
		double a=0,b;
		if (context.getStack().isEmpty()) throw new EmptyStack();
		a=context.getStack().peekFirst();
		context.getStack().pop();
		if (context.getStack().isEmpty()) {
			context.getStack().addFirst(a);
			throw new EmptyStack();
		}
		b=context.getStack().peekFirst();
		context.getStack().pop();
		context.getStack().addFirst(a+b);
	}
} 
