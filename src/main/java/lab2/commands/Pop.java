package lab2;

import java.util.logging.Logger;

class Pop extends Command {
	private static Logger log=Logger.getLogger(Pop.class.getName());
	void make (String[] argc,Context context) throws EmptyStack {
		log.info(argc.toString());
		if (context.getStack().isEmpty()) throw new EmptyStack();
		context.getStack().pop();
		log.info("Pop was completed");
	}
}
