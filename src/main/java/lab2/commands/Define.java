package lab2;

import java.util.logging.Logger;

class Define extends Command {
	private static Logger log=Logger.getLogger(Define.class.getName());
	void make (String [] argc,Context context) throws IncorrectArg {
		log.info(argc.toString());
		if (argc.length<3 || !argc[2].matches("-?\\d+(.\\d+)?")) throw new IncorrectArg();
		context.getPerems().put(argc[1], Double.valueOf(argc[2]));
		log.info("Define completed");
	}
}
