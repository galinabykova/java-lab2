package lab2;

import java.util.logging.Logger;

class Sqrt extends Command {
	private static Logger log=Logger.getLogger(Sqrt.class.getName());
	void make (String [] argc,Context context) throws EmptyStack, InvalidStackNumberForCommand {
		log.info(argc.toString());
		if (context.getStack().isEmpty()) throw new EmptyStack();
		double a=context.getStack().peekFirst();
		if (a<0) throw new SqrtOfNegative(a);
		context.getStack().pop();
		context.getStack().addFirst(Math.sqrt(a));
		log.info("Sqrt was completed");
	}
} 
