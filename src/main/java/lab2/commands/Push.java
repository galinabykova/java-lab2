package lab2;

import java.util.logging.Logger;

class Push extends Command {
	private static Logger log=Logger.getLogger(Push.class.getName());
	void make (String[] argc,Context context) throws WrongCountOfArgs, IncorrectArg {
		log.info(argc.toString());
		if (argc.length==1) throw new WrongCountOfArgs();
		if (context.getPerems().containsKey(argc[1])) {
			context.getStack().addFirst(context.getPerems().get(argc[1]));
		} else {
			if (argc[1].matches("-?\\d+(.\\d+)?")) {
				context.getStack().addFirst(Double.valueOf(argc[1]));
			} else {
				throw new IncorrectArg();
			}
		}
		log.info("Push was completed");
	}
}
