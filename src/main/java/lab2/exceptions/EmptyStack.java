package lab2;

public class EmptyStack extends StackException {
	private static final long serialVersionUID = 1L;
	public String getMessage() {
		return "Few numbers on the stack";
	}
}
