package lab2;

public class DivByZero extends InvalidStackNumberForCommand {
	private static final long serialVersionUID = 1L;
	public String getMessage() {
		return "Division by zero";
	}
}
