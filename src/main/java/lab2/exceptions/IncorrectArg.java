package lab2;

public class IncorrectArg extends InputException {
	private static final long serialVersionUID = 1L;
	public String getMessage() {
		return "Invalid Command Arguments";
	}
}
