package lab2;

public class WrongCountOfArgs extends InputException {
	private static final long serialVersionUID = 1L;
	public String getMessage() {
		return "Invalid number of command arguments";
	}
}
