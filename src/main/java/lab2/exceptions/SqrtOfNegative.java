package lab2;

public class SqrtOfNegative extends InvalidStackNumberForCommand {
	private static final long serialVersionUID = 1L;
	double number;
	public SqrtOfNegative (double number) {
		this.number=number;
	}
	public String getMessage() {
		return "Trying to extract the root from" + number;
	}
}
