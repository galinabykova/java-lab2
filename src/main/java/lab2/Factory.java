package lab2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

class Factory {
	private static Logger log=Logger.getLogger(Factory.class.getName());
	static Map <String,Class<? extends Command>> map=new HashMap <String,Class<? extends Command>>();
	Context context=new Context();
	Factory () {
		log.info("Factory began to create");
		BufferedReader res=null;
		try {
			res = new BufferedReader(new InputStreamReader(Factory.class.getResourceAsStream("config.txt")));
			log.info("Config.txt was opened");
			String s=res.readLine();
			log.info("\""+s+"\" was read");
			while (s!=null) {
				try {
					String[] arr=s.split(" ");
					Class<?> c= Class.forName(arr[1]);
					if (!(Class.forName("lab2.Command").isAssignableFrom(c))) {
						log.severe("Class does not extends Command");
					}
					@SuppressWarnings("unchecked")  //???
					Class<? extends Command> d=(Class<? extends Command>)c;
					map.put(arr[0],d);
					log.info("\""+s+"\" was processed");
				} catch (ClassNotFoundException e) {
					log.severe("Class not found");
				} finally {
					s=res.readLine();
					log.info("\""+s+"\" was read");
				}
			}
		} catch (IOException e) {
			log.severe("IOException when opening config.txt");
		}  
		finally
		{
			if (null != res) {
				try {
					res.close();
					log.info("Config.txt was closed");
				}
				catch (IOException e) {
					log.severe("IOException when closing config.txt");
				}
			}
		}
	}
	void make (String s) {
		log.info("make:");
		String[] argc=s.split(" ");
		Class<?> c=map.get(argc[0]);
		try {
			if (argc[0].startsWith("#")) argc[0]="#";
			if (c==null) {
				log.info("Incorrect command: "+argc[0]);
				return;
			}
			Command a=(Command) c.newInstance();
			log.info("Command was created");
			a.make(argc,context);
		} catch (InstantiationException e) {
			log.severe("Class"+c.getName()+"hasn't Class()");
		} catch (IllegalAccessException e) {
			log.severe("Error. Maybe, not access to Class() "+c.getName());
		} catch (MyException e) {
			log.info(e.getMessage());
		}
	}
}
