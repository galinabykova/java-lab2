package lab2;

import java.io.*;
import java.util.logging.*;


public class Lab2 {
	private static Logger log=Logger.getLogger(Lab2.class.getName());
	public static void main (String [] argc) {
		try {
			LogManager.getLogManager().readConfiguration(Lab2.class.getResourceAsStream("logging.properties"));
		} catch (IOException e) {
			System.err.println("doesn't setup logging.properties");
		}
		log.info("Start!");
		BufferedReader reader=null;
		try {
			if (argc.length==0) reader = new BufferedReader(new InputStreamReader(System.in));
			else reader = new BufferedReader(new FileReader(argc[0]));
			log.info("InputFile was opened");
			Factory factory=new Factory();
			log.info("Factory was created");
			String s=reader.readLine();
			log.info("\""+s+"\" was read");
			while (s!=null) {
				factory.make(s);
				log.info("\tfactory finished to make");
				s=reader.readLine();
				log.info("\""+s+"\" was read");
			} 
		} catch (IOException e) {
			//System.err.println("IOException");
			log.severe("IOException");
		}
		finally
		{
			if (null != reader) {
				try {
					reader.close();
					log.info("InputFile was closed");
				}
				catch (IOException e) {
					log.severe("IOException when closing file");
				}
			}
		}
	}
}
