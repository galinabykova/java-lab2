package lab2;

abstract  public class Command {
	abstract void make (String[] argc,Context context) throws MyException;
}
